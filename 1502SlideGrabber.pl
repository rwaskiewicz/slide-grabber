# Title: 1502SlideGrabber.pl
# Author: Ryan Waskiewicz
# Date: 9/23/2013
# Version 1
# Purpose: the purpose of this script is to grab slides embedded in the CS 1502 page 
# so that they can be (externally) put into a pdf file for easy reading

use warnings;
use strict;
use LWP::Simple;

if (!$ARGV[0])
{
	print("Usage: perl NAME.pl <lecture number to grab>\n");
	exit -1;
}

my $lectureToGrab = $ARGV[0];			#lecture from which to grab slides
my $slideToGrab = "001";				#next slide number to grab
my $getStatus = 0;						#status of the last get command

#ensure the format of lectures 1-9 are formatted properly
if ($lectureToGrab < 10)
{
	$lectureToGrab =~ s/^0*//;
	$lectureToGrab = "0" . $lectureToGrab;
}

#check to see if lecture is posted, if not quit (don't make dir)
$getStatus = get("http://busybeaver.cs.pitt.edu/courses/lectures/cs1502/Lecture" .
				 "$lectureToGrab/cs1502-Lecture$lectureToGrab.$slideToGrab.jpg");
if (!$getStatus)
{
	print("Lecture not yet available\n");
	exit -1;
}

#init save location
my $saveLocation = "/Users/Ryan/Dropbox/Pitt/Junior/Fall 2013/Formal Methods in CS/" .
				   "Slides/Lecture$lectureToGrab";
mkdir $saveLocation unless (-d $saveLocation);

while($getStatus != 404)
{
	#get and store the slide image
	$getStatus = getstore("http://busybeaver.cs.pitt.edu/courses/lectures/cs1502/Lecture$lectureToGrab".
						  "/cs1502-Lecture$lectureToGrab.$slideToGrab.jpg",
			              "$saveLocation/$slideToGrab.png");
	$slideToGrab += 1;
	
	#format the next slide so that all slide numbers use 3 digits
	while(length($slideToGrab) < 3) {
		substr($slideToGrab, 0, 0) = '0';
	}
}
