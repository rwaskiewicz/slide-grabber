Slide-Grabber
=============

Quick script for grabbing CS 1502 slides.

Requires LWP::Simple

Usage:
perl 1502SlideGrabber.pl <Lecture Number to Grab>

Script will do all lecture and slide number formattings

You must edit the saveLocation scalar to specify where the PNG files are saved
